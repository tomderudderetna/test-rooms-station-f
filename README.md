# Dépôt test-rooms-station-f
Author      Tom De Rudder  
Version     3.0.7
___
> "_test-rooms-station-f_" est ma réponse au test technique de StationF.

## Architecture
```bash
	test-rooms-station-f
	├── api # api avec express
    │	├── config / # configuration (hôte, port, nom de la database)
    │	├── app.js
	│	├── package.json
	│	└── roots.js
	│	
	├── db
    │   └── rooms.json # fichier donné par stationF
	├── misc / # croquis
	├── render / # aperçue du site
    │
    ├── site # site en react
	│	├── .env
	│	├── package.json
	│	├── public
	│	├── README.md
	│	└── src
    │
	├── source.md # mes recherces web pour ce projet
	└── README.md
```

## Pré requis
- Node.js
- MongoDB

## Installation
```bash
    git clone "https://gitlab.com/tomderudderetna/test-rooms-station-f.git" stationF
    cd ./stationF/db
    mongoimport --db stationF --collection rooms --file rooms.json --jsonArray
```

## Liens utiles
 - [voir le fichier source.md](./source.md)
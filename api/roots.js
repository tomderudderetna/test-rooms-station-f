'use strict';

module.exports = (mongodb, app, db) => {

    const ObjectID = mongodb.ObjectID
        , Rooms = db.collection("rooms")
        , Reservations = db.collection("reservations");

    app.get('/rooms', (req, res) =>
        Rooms.find()
            .toArray((err, rooms) => {
                if (err) throw err;

                res.send(rooms);
            }));

    app.get('/capacity-max', (req, res) =>
        Rooms.find()
            .project({ capacity: 1, _id: 0 })
            .toArray((err, capacities) => {
                if (err) throw err;

                capacities = capacities.map(capacity => capacity.capacity)
                res.send({ 'max': Math.max(...capacities) });
            }));


    app.get('/equipements', (req, res) =>
        Rooms.find({})
            .project({ equipements: 1, _id: 0 })
            .toArray((err, result) => {
                if (err) throw err;

                const equipementsList = [];
                result.forEach(equipements =>
                    equipements.equipements.forEach(equipement =>
                        equipementsList.indexOf(equipement.name) == -1 && equipementsList.push(equipement.name)))

                res.send(equipementsList);
            }));

    app.get('/reservations', (req, res) =>
        Reservations.find().toArray((err, reservations) => {
            if (err) throw err;

            res.send(reservations);
        }));

    app.post('/room/search', (req, res) => {
        if (typeof req.body != 'object')
            return failure(res, 'Content-Type must be application/json');

        if (!req.body ||
            isNaN(req.body.capacity) ||
            !req.body.equipements ||
            !req.body.dateBegin ||
            !req.body.dateEnd)
            return failure(res, 'Body params are not correct');

        const capacityPost = parseInt(req.body.capacity)
        if (capacityPost <= 0)
            return failure(res, 'capacity is not correct');

        const equipements = req.body.equipements.map(name => ({ name: name }));
        const data = {
            capacity: capacityPost,
            dateBegin: new Date(req.body.dateBegin),
            dateEnd: new Date(req.body.dateEnd)
        };

        const roomConditions = {
            capacity: { $gte: data.capacity }
        };

        if (equipements.length > 0) roomConditions.equipements = { $all: equipements };

        /*
         * Check if the room has sufficient capacity
         * and if the room has the necessary material
         */
        Rooms.find(roomConditions).toArray((err, rooms) => {
            if (err) throw err;

            const reservationConditions = {
                roomId: { $in: rooms.map(room => new ObjectID(room._id)) },
                $or: [
                    {
                        $or: [
                            { "dateBegin": { $gte: data.dateBegin, $lt: data.dateEnd } },
                            { "dateEnd": { $gt: data.dateBegin, $lte: data.dateEnd } }
                        ]
                    },
                    {
                        "dateBegin": { $lte: data.dateBegin },
                        "dateEnd": { $gte: data.dateEnd }
                    }
                ]
            };

            /*
             * Check if the piece is free at this time
             */
            Reservations.find(reservationConditions).toArray((err, reservations) => {
                if (err) throw err;

                rooms = rooms.filter(room =>
                    reservations.find(reservation =>
                        reservation.roomId.toString() == room._id.toString()) === undefined);

                res.send(rooms);
            });
        });
    });

    app.post('/reservation/add', (req, res) => {
        if (!req.body || !req.body.roomId || !req.body.dateBegin || !req.body.dateEnd)
            return failure(res, 'body params are not correct');

        const data = {
            roomId: new ObjectID(req.body.roomId),
            dateBegin: new Date(req.body.dateBegin),
            dateEnd: new Date(req.body.dateEnd)
        };

        Rooms.countDocuments({
            _id: new ObjectID(req.body.roomId)
        }, (err, nbRooms) => {

            if (nbRooms == 0)
                return failure(res, 'room dont exist');

            Reservations.countDocuments({
                roomId: new ObjectID(req.body.roomId),
                $or: [
                    {
                        $or: [
                            { "dateBegin": { $gte: data.dateBegin, $lt: data.dateEnd } },
                            { "dateEnd": { $gt: data.dateBegin, $lte: data.dateEnd } }
                        ]
                    },
                    {
                        "dateBegin": { $lte: data.dateBegin },
                        "dateEnd": { $gte: data.dateEnd }
                    }
                ]
            }, (err, nbReservations) => {

                if (nbReservations != 0)
                    return failure(res, 'reservation already exist');

                Reservations.insertOne(data, (err, reservation) => {
                    if (err) throw err;
                    res.send({
                        code: 200,
                        feedback: 'success'
                    });
                });
            });
        });
    });

    app.use((req, res, next) => {
        res.status(404).send({
            code: 404,
            message: 'Not Found'
        });
    });

    app.use((err, req, res, next) => {
        if (res.headersSent)
            return next(err);

        res.status(500).send({
            code: 500,
            message: 'Internal Server Error'
        });
    });
}

function failure(res, message) {
    return res.status(400).send({
        code: 400,
        feedback: 'failure',
        message: message
    });
}
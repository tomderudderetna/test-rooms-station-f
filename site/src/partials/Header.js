import React, { Component } from 'react';
import './Header.css';

class Header extends Component {

    render() {
        return (
            <header className="center pos-rel center">
                <img id="plan" alt="plan" src="/img/logo.png" />
                <h1 className="up">Gestion des salles de réunions</h1>
                <span className="btn btn-lg btn-round btn-green btn-border up pos-bot-center">version 3.0.7</span>
            </header>
        )
    }
}

export default Header;
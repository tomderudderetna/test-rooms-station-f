import React, { Component } from 'react';

class Plan extends Component {
    constructor(props) {
        super(props);

        this.back = new Image();
        this.wall = new Image();
        this.displayStationF = this.displayStationF.bind(this);
    }

    componentDidMount() {
        this.canvas = document.getElementById('canvas');
        this.ctx = this.canvas.getContext("2d");
        this.back.src = '/img/plan/back.png';
        this.props.rooms.forEach(room => room.img.src = 'img/plan/' + room.id + '.png');
        this.wall.src = '/img/plan/wall.png';
        window.requestAnimationFrame(this.displayStationF);
    }

    displayStationF() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.drawImage(this.back, 0, 0);
        this.props.rooms.forEach(room => room.display && this.ctx.drawImage(room.img, 0, 0));
        this.ctx.drawImage(this.wall, 0, 0);
        window.requestAnimationFrame(this.displayStationF);
    }

    render() {
        return (
            <canvas id="canvas" height="729" width="845"></canvas>
        );
    }
}

export default Plan;
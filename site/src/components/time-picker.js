import React, { Component } from 'react';

class TimePicker extends Component {

    render() {
        return (
            <label className="pd-sm">
                <span className=" up">{this.props.children}</span>
                <input type="time" value={this.props.value} onChange={this.props.parentMethod} />
            </label>
        );
    }
}

export default TimePicker;
import React, { Component } from 'react';
import { DateRange } from 'react-date-range';
import locale from 'date-fns/locale/fr';
import './styles.css';
import './date-picker.css';

class DatePicker extends Component {

    render() {
        const selectionRange = {
            startDate: new Date(this.props.startDate),
            endDate: new Date(this.props.endDate),
            key: 'selection',
        }
        return (
            <DateRange
                rangeColors="#1abc9c"
                color="#1abc9c"
                locale={locale}
                showDateDisplay={false}
                ranges={[selectionRange]}
                onChange={this.props.parentMethod}
            />
        )
    }
}

export default DatePicker;

import React, { Component } from 'react';

class ListRoomsItem extends Component {

    render() {
        return (
            <div key={this.props.id} className="list-item">
                <div className="row">
                    <div className="col">
                        {this.props.room.name}
                    </div>
                    <div className="col">
                        <a className="btn btn-green btn-round" onClick={this.props.parentMethod.bind(this, this.props.room._id)} >reserver</a>
                    </div>
                </div>
                <div className="row">
                    {this.props.room.equipements.map((equipement, key) =>
                        <a className="btn btn-stroke equipement" key={key}>{equipement.name}</a>
                    )}
                </div>
            </div>
        );
    }
}

export default ListRoomsItem;
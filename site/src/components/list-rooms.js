
import React, { Component } from 'react';
import ListRoomsItem from './list-rooms-item.js';

class ListRooms extends Component {

    render() {
        return (
            <div className="list">
                {this.props.rooms.map((room, key) =>
                    <ListRoomsItem key={key} room={room} parentMethod={this.props.parentMethod} />
                )}
            </div>
        );
    }
}

export default ListRooms;
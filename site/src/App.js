import React, { Component } from 'react';
import Header from './partials/Header';
import ListEquipements from './components/list-equipements.js';
import ListRooms from './components/list-rooms.js';
import DatePicker from './components/date-picker.js';
import TimePicker from './components/time-picker.js';
import Range from './components/range.js';
// import 'react-date-range/dist/styles.css';
import Plan from './components/plan.js';
import './App.css';
// import { library } from '@fortawesome/fontawesome-svg-core'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faSearch } from '@fortawesome/free-solid-svg-icons'

// library.add(faSearch)

class App extends Component {

  constructor(props) {
    super(props);

    const now = new Date();

    this.state = {
      dateBegin: now,
      dateEnd: new Date(Date.now() + 3600000),
      timeStart: ('0' + now.getHours()).slice(-2) + ':' + ('0' + now.getMinutes()).slice(-2),
      timeEnd: ('0' + (now.getHours() + 1)).slice(-2) + ':' + ('0' + now.getMinutes()).slice(-2),
      capacity: 1,
      capacityMax: 1,
      equipements: ["TV", "Retro Projecteur"],
      equipementsSelected: [],
      roomsSearch: [],
      roomsSchema: [
        {
          id: '5b698c7aceca80eabd45f9c4',
          img: new Image(),
          display: false
        },
        {
          id: '5b698c7aceca80eabd45f9c5',
          img: new Image(),
          display: false
        },
        {
          id: '5b698c7aceca80eabd45f9c6',
          img: new Image(),
          display: false
        },
        {
          id: '5b698c7aceca80eabd45f9c7',
          img: new Image(),
          display: false
        },
        {
          id: '5b698c7aceca80eabd45f9c8',
          img: new Image(),
          display: false
        },
      ],
      roomsAvailable: []
    }
    this.changeDateRange = this.changeDateRange.bind(this);
    this.changeTimeStart = this.changeTimeStart.bind(this);
    this.changeTimeEnd = this.changeTimeEnd.bind(this);
    this.changeEquipements = this.changeEquipements.bind(this);
    this.changeCapacity = this.changeCapacity.bind(this);
    this.searchRoom = this.searchRoom.bind(this);
    this.reserveRoom = this.reserveRoom.bind(this);
  }

  componentDidMount() {
    fetch("http://localhost:3000/capacity-max")
      .then(res => res.json())
      .then(max => {
        this.setState({
          capacityMax: max.max
        })
      },
        err => console.error(err));
  }

  changeDateRange(ranges) {
    const valueStart = ranges.selection.startDate;
    const valueEnd = ranges.selection.endDate;
    const dateStart = this.state.dateBegin;
    const dateEnd = this.state.dateEnd;
    dateStart.setFullYear(valueStart.getFullYear())
    dateStart.setMonth(valueStart.getMonth())
    dateStart.setDate(valueStart.getDate())
    dateEnd.setFullYear(valueEnd.getFullYear())
    dateEnd.setMonth(valueEnd.getMonth())
    dateEnd.setDate(valueEnd.getDate())
    this.setState({
      dateBegin: dateStart,
      dateEnd: dateEnd
    }, () =>
        this.searchRoom());
  }

  changeTimeStart(event) {
    const value = event.target.value;
    const valueSplied = value.split(':');
    const dateStart = this.state.dateBegin;
    dateStart.setHours(valueSplied[0] || 0)
    dateStart.setMinutes(valueSplied[1] || 0)
    this.setState({
      timeStart: value,
      dateBegin: dateStart
    }, () =>
        this.searchRoom());
  }

  changeTimeEnd(event) {
    const value = event.target.value;
    const valueSplied = value.split(':');
    const dateEnd = this.state.dateEnd;
    dateEnd.setHours(valueSplied[0] || 0)
    dateEnd.setMinutes(valueSplied[1] || 0)
    this.setState({
      timeEnd: event.target.value,
      dateEnd: dateEnd
    }, () =>
        this.searchRoom());
  }

  changeEquipements(id) {
    if (id === -1) {
      this.setState(state => ({
        equipementsSelected: !this.state.equipementsSelected.length ?
          state.equipements : []
      }), () =>
          this.searchRoom());
    }
    else
      this.setState(state => {
        state.equipementsSelected.indexOf(state.equipements[id]) >= 0 ?
          state.equipementsSelected = state.equipementsSelected.filter((equipement, index) => equipement !== state.equipements[id]) :
          state.equipementsSelected.push(state.equipements[id]);
        return (
          {
            equipementsSelected: state.equipementsSelected
          });
      }, () =>
          this.searchRoom());
  }

  changeCapacity(event) {
    this.setState({ capacity: event.target.value }, () =>
      this.searchRoom());
  }

  updateRoomsAvailableList() {
    this.setState(state => ({
      roomsAvailable: state.roomsSearch
    }));
  }
  updatePlan() {
    const rooms = this.state.roomsSearch;
    this.setState(state => {
      state.roomsSchema.forEach(room => room.display = rooms.findIndex(element => element._id === room.id) === - 1)
      return { roomsSchema: state.roomsSchema }
    })
  }

  searchRoom(event) {
    event && event.preventDefault();
    const body = JSON.stringify({
      capacity: this.state.capacity,
      equipements: this.state.equipementsSelected,
      dateBegin: this.state.dateBegin,
      dateEnd: this.state.dateEnd
    });

    fetch("http://localhost:3000/room/search", {
      method: "POST",
      headers: { 'Content-Type': 'application/json' },
      body: body
    })
      .then(res => res.json())
      .then(data => {
        this.setState({ roomsSearch: data });
        this.updateRoomsAvailableList()
        this.updatePlan()
      })
  }

  reserveRoom(roomId) {
    const body = JSON.stringify({
      roomId: roomId,
      dateBegin: this.state.dateBegin,
      dateEnd: this.state.dateEnd
    });

    fetch("http://localhost:3000/reservation/add", {
      method: "POST",
      headers: { 'Content-Type': 'application/json' },
      body: body
    })
      .then(res => res.json())
      .then(data => {
        this.setState(state => {
          state.roomsSearch = state.roomsSearch.filter(room => room._id !== roomId)
          return { roomsSearch: state.roomsSearch }
        });
        this.updateRoomsAvailableList()
        this.updatePlan()
      })
  }

  render() {
    return (
      <div>
        <Header />
        <section id="searchPanel" className="mg-lg">
          <form name="search">
            <div className="row">

              <div className="col-4">
                <DatePicker startDate={this.state.dateBegin} endDate={this.state.dateEnd} parentMethod={this.changeDateRange} />
              </div>

              <div className="col-2"></div>
              <div className="col-6">
                <div className="row">
                  <div className="col-4">
                    <TimePicker value={this.state.timeStart} parentMethod={this.changeTimeStart}>début</TimePicker>
                  </div>
                  <div className="col-4">
                    <TimePicker value={this.state.timeEnd} parentMethod={this.changeTimeEnd}>fin</TimePicker>
                  </div>
                  <div className="col-4 flex">
                    <Range value={this.state.capacity} max={this.state.capacityMax} parentMethod={this.changeCapacity}>effectif souhaités</Range>
                    {this.state.capacity}
                  </div>
                </div>
                <div className="row">
                </div>
                <ListEquipements selection={this.state.equipementsSelected} parentMethod={this.changeEquipements} />
              </div>

            </div>
          </form>
        </section >

        <section id="resultPanel" className="mg-lg">
          <div className="row">
            <div className="col">
              <Plan rooms={this.state.roomsSchema} />
            </div>
            <div className="col">
              <ListRooms rooms={this.state.roomsAvailable} parentMethod={this.reserveRoom} />
            </div>
          </div>
        </section>

      </div >
    );
  }
}

export default App;